<?php
	// include JobtraqAPI.php file
	require_once "JobtraqAPI.php";
	echo '<pre>';
	$arr = JobtraqAPI::createModule([
				'MODULE'=>'quotation',
	          	'DATA' => 
	          		array (
  'delivery_method' => ' Purolator Prepaid',
  'code' => '15-05-17-VH-531',
  'company_id' => '',
  'shipping_address' => 
  array (
    0 => 
    array (
      'shipping_address_1' => '2915 - 10th Avenue NE',
      'shipping_address_2' => '',
      'shipping_address_3' => '',
      'shipping_country' => 'Canada',
      'shipping_country_id' => 'CA',
      'shipping_default' => 1,
      'deleted' => false,
      'shipping_name' => '',
      'shipping_name_id' => '',
      'shipping_contact_name' => 'Van Houtte Coffee Services-Calgary',
      'shipping_province_state' => 'Alberta',
      'shipping_province_state_id' => 'AB',
      'shipping_town_city' => 'Calgary',
      'shipping_zip_postcode' => 'T2A 5L4',
    ),
  ),
  'company_name' => 'Van Houtte Coffee Services',
  'company_phone' => '',
  'contact_name' => 'Gene Grimm',
  'currency' => 'cad',
  'contact_id' => '',
  'customer_po_no' => '22121-',
  'date_modified' => '',
  'deleted' => false,
  'description' => '',
  'email' => '',
  'payment_due_date' => '',
  'invoice_address' => 
  array (
    0 => 
    array (
      'invoice_address_1' => 'Montreal A/P',
      'invoice_address_2' => '',
      'invoice_address_3' => '3700 Jean - Rivard',
      'invoice_country' => 'Canada',
      'invoice_country_id' => 'CA',
      'invoice_default' => 1,
      'deleted' => false,
      'invoice_name' => '',
      'invoice_name_id' => '',
      'invoice_contact_name' => 'Van Houtte Coffee Services Inc-Calgary',
      'invoice_province_state' => 'Quebec',
      'invoice_province_state_id' => 'QC',
      'invoice_town_city' => 'Montreal',
      'invoice_zip_postcode' => 'H1Z 4J1',
    ),
  ),
  'products' => 
  array (
    0 => 
    array (
      'code' => 'WT-VHT-104',
      'sku' => 'VM2757',
      'products_name' => 'Mural with VH Wordmark 10\' - Option B',
      'products_id' => 
      array (
        '$id' => '5515b07e6803fa30d98b4568',
      ),
      'quantity' => 1,
      'sub_total' => 440,
      'option_group' => '',
      'sizew' => 120,
      'sizew_unit' => 'in',
      'sizeh' => 48,
      'sizeh_unit' => 'in',
      'sell_by' => 'unit',
      'oum' => 'unit',
      'same_parent' => 0,
      'sell_price' => 440,
      'taxper' => 0,
      'tax' => 0,
      'deleted' => false,
      'proids' => '0',
      'adj_qty' => 0,
      'oum_depend' => 'unit',
      'unit_price' => 440,
      'amount' => 440,
    ),
    1 => 
    array (
      'deleted' => false,
      'product_name' => 'Shipping and Handling (Purolator)',
      'products_costing_name' => 'Shipping and Handling (Purolator)',
      'products_id' => '529eae6167b96dc11d00005c',
      'option' => '',
      'sizew' => 0,
      'sizew_unit' => '',
      'sizeh' => 0,
      'sizeh_unit' => '',
      'receipts' => '',
      'sell_by' => '',
      'sell_price' => 0,
      'oum' => '',
      'unit_price' => 0,
      'quantity' => 1,
      'adj_qty' => 1,
      'sub_total' => 0,
      'taxper' => 0,
      'amount' => 0,
      'sku' => 'SHP-998',
      'code' => '998',
      'is_custom_size' => 0,
      'custom_unit_price' => 0,
      'tax' => 0,
      'plus_sell_price' => 0,
      'oum_depend' => '',
      'plus_unit_price' => 0,
      'company_price_break' => false,
    ),
  ),
  'options' => 
  array (
  ),
  'sales_order_type' => 'Sales Order',
  'status' => '',
  'status_id' => '',
  'sum_amount' => 440,
  'sum_sub_total' => 440,
  'wt_information' => 
  array (
    'location_id' => 10004,
    'type_order' => 0,
    'order_ref' => '3adad',
    'user_name' => 'Gene Grimm',
    'term' => 0,
    'cost_kitting' => 35,
    'total_kitting' => 0,
    'items' => 
    array (
      0 => 
      array (
        'order_item_id' => 2282,
        'order_id' => 531,
        'arr_jt_information' => 
        array (
        ),
        'product_id' => 104,
        'kit_content' => 
        array (
        ),
        'weight' => 0.01,
        'weight_size' => 'lb',
        'shipping_cost' => 0,
        'shipping_cost_type' => '',
        'use_inventory' => 0,
        'minimum_quantity' => 1,
        'package_type' => 0,
        'company_id' => 10004,
        'location_id' => 10004,
        'product_code' => 'VM2757',
        'original_id' => 0,
        'product_description' => 'Mural with VH Wordmark 10\' - Option B',
        'quantity' => 1,
        'description' => 'Already allocated',
        'customization_information' => '',
        'width' => 120,
        'length' => 48,
        'unit' => 'in',
        'product_size_option' => 0,
        'current_size_option' => 0,
        'graphic_file' => 'http://jobtraq.anvyonline.com/upload/2017_05/2017_05_11_102617_563082.jpg',
        'graphic_id' => 0,
        'product_image_option' => 0,
        'current_image_option' => 0,
        'custom_image_path' => '',
        'original_price' => 0,
        'discount_price' => 0,
        'current_price' => 440,
        'unit_price' => 0,
        'status' => 0,
        'discount_type' => 0,
        'discount_per_unit' => 0,
        'net_price' => 0,
        'extended_amount' => 0,
        'material_id' => 8,
        'material_name' => 'Adhesive Vinyl',
        'material_color' => 'white',
        'material_thickness_value' => 0,
        'material_thickness_unit' => 'in',
        'total_price' => 440,
        'product_branch_index' => -1,
        'product_text_option' => 0,
        'current_text_option' => 0,
        'text' => 
        array (
        ),
        'design_id' => 0,
        'upload_key_id' => 0,
        'allocation' => 
        array (
          0 => 
          array (
            'location_id' => 10004,
            'location_name' => 'Calgary',
            'location_address' => 'Bay 1 2915 - 10th Avenue NE Calgary Alberta T2A 5L4 ',
            'location_quantity' => 1,
            'threshold' => 'y:-1',
            'location_price' => '440',
            'product_id' => 104,
            'product_image' => 'http://jobtraq.anvyonline.com//upload/2017_05/2017_05_11_102617_563082.jpg',
            'design_id' => 0,
            'weight' => '',
            'weight_size' => '',
            'product_branch_index' => -1,
            'product_name' => 'Mural with VH Wordmark 10\' - Option B 120\'\' × 48\'\'   in Adhesive Vinyl-- Color: white',
            'tracking_url' => '',
            'tracking_number' => '',
            'tracking_company' => '',
            'date_shipping' => NULL,
            'shipping_status' => 0,
          ),
        ),
      ),
    ),
  ),
)
	          
		]);


	print_r($arr);
?>