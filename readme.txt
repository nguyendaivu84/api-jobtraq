JOBTRAQ API DOCUMENT



A - Setup:

1. Add key for your company: 
+ login Jobtraq project
+ Link to http://jt.anvy.net/apis/lists
+ Add new key and choice your company

2. Setup  API:
+ Open JobtraqAPI.php
+ Change line 3:
	private static $apiKey = 'YOUR NEW KEY'
+ Setup server jobtraq (line 4):  
	private static $host = 'http://jt.anvy.net/api';

3. Use: open demo.php and setup options for $arr_data




B - API Functions :

listModule($arr_data): list view

detailModule($arr_data): view detail of record

deleteModule($arr_data):  delete record




C - Option of $arr_data :

MODULE : quotation, salesorder, salesinvoice, company, contact, company

LIMIT : limit record return, max is 3000

PAGE  : page, cause we dont return all records of table, we use page instead

FIELD : extra field return, use detail first to get field name of each module
    example: FIELD => array('company_id', 'company_name', 'contact_name')

WHERE : condition when list
    example: WHERE => array('company_name' => COMPANY, 'contact_name' => CONTACT)
     means company_name = COMPANY, contact_name = CONTACT
    example: WHERE => arraycontact_id => new MongoId(mongoId))
     means contact_id = new MongoId(mongoId)