<?php
class JobtraqAPI{
	private static $apiKey = '59127ef66803fad77a3d14dd';
	private static $host = 'http://50.99.42.160/api';
	private static $arrPost = array();
	private static $isJSON = false;
	function __construct($apiKey = '', $host = '')
	{
		if($apiKey)
			static::$apiKey = $apiKey;
		if($host)
			static::$host = $host;
	}

	public static function getDefaultPost($action)
	{
		static::$arrPost = array(
		          'API'		=> static::$apiKey,
		          'ACTION'	=> $action
		          );
	}

	public static function connect1()
	{
		$curl = curl_init(static::$host);
		if(static::$isJSON){
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(static::$arrPost));
		} else{
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(static::$arrPost));
		}
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_URL, static::$host );
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		// curl_setopt($curl, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
		curl_setopt($curl, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		$response  = curl_exec($curl);
		
		$curl_errno = curl_errno($curl);
        $curl_error = curl_error($curl);
        if ($curl_errno > 0) {
                echo "cURL Error ($curl_errno): $curl_error\n";
        } else {
                echo "Data received\n";
        }

        print_r($response);

		curl_close($curl);
		return $response;
	}

	public static function connect()
	{
		$curl = curl_init(static::$host);
		if(static::$isJSON){
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(static::$arrPost));
		} else{
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(static::$arrPost));
		}
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		$response  = curl_exec($curl);
		
		$curl_errno = curl_errno($curl);
        $curl_error = curl_error($curl);
        if ($curl_errno > 0) {
                echo "cURL Error ($curl_errno): $curl_error\n";
        } else {
                echo "Data received\n";
        }

        print_r($response);

		curl_close($curl);
		return $response;
	}

	public static function getData()
	{
		$response = static::connect();
		return $response = json_decode($response, true);
	}

	public static function listModule($arr_data)
	{
		static::$isJSON = true;
		static::getDefaultPost('list');
		static::$arrPost  = array_merge(static::$arrPost ,$arr_data);
		return static::getData();
	}

	public static function detailModule($arr_data)
	{
		if(!isset($arr_data['_ID']) && $arr_data['MODULE'] != 'Company')
			return array('Message' => 'Missing _ID');
		static::getDefaultPost('detail');
		static::$arrPost  = array_merge(static::$arrPost ,$arr_data);
		return static::getData();
	}

	public static function deleteModule($arr_data)
	{
		if(!isset($arr_data['_ID'])){
			if(!isset($arr_data['WHERE']))
				return array('Message' => 'Missing _ID');
		}
		static::getDefaultPost('delete');
		static::$arrPost  = array_merge(static::$arrPost ,$arr_data);
		return static::getData();
	}

	public static function createModule($arr_data)
	{
		if(!isset($arr_data['DATA']))
			return array('Message' => 'Missing DATA');
		static::$isJSON = true;
		static::getDefaultPost('create');
		static::$arrPost  = array_merge(static::$arrPost ,$arr_data);
		return static::getData();
	}

	public static function updateModule($arr_data)
	{
		if(!isset($arr_data['_ID']))
			return array('Message' => 'Missing _ID');
		if(!isset($arr_data['DATA']))
			return array('Message' => 'Missing DATA');
		static::$isJSON = true;
		static::getDefaultPost('update');
		static::$arrPost  = array_merge(static::$arrPost ,$arr_data);
		return static::getData();
	}

	public static function reportModule($arr_data)
	{
		if(!isset($arr_data['_ID']))
			return array('Message' => 'Missing _ID');
		static::getDefaultPost('report');
		static::$arrPost  = array_merge(static::$arrPost ,$arr_data);
		return static::getData();
	}

	public static function statusModule($arr_data)
	{
		static::getDefaultPost('status');
		static::$arrPost  = array_merge(static::$arrPost ,$arr_data);
		return static::getData();
	}

	public static function pricing($arr_data)
	{
		if(!isset($arr_data['DATA']))
			return array('Message' => 'Missing DATA');
		static::$isJSON = true;
		static::getDefaultPost('pricing');
		static::$arrPost  = array_merge(static::$arrPost, $arr_data, array('MODULE' => 'pricing'));
		return static::getData();
	}
}